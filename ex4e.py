notas  = [
    [1 ,11, 14, 19],
    [1 ,11, 14, 19],
    [14, 13, 15, 19],
    [14, 13, 15, 19],
    [14, 13, 15, 19],
    [14, 13, 15, 19],
    [1, 5, 3, 2],
    [1, 5, 3, 2],
    [12, 13, 1, 1],
    [16, 12, 1, 1],
]

medias = []
for notas_aluno in notas:
    medias.append(sum(notas_aluno) / len(notas_aluno))

print(medias)

positiva = 0
for m in medias:
    if m >= 9.5:
        positiva += 1

print('positivas: {}, negativas:{}'.format(positiva, len(medias) - positiva))