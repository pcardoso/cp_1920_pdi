# CP/PDI: 3(l)
# A série de Fibonacci é formada pela sequência 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...
# Faça um programa que gere a série até que o valor seja maior que 500.

limite = 500

# solucao 1
a, b = 1, 1
print('1 1', end='')
while limite >= b:
    a, b = b, a + b
    if b < limite:
        print(' {}'.format(b), end='')


# solucao 2
a, b = 1, 1
print('\n1 1', end='')
while True:
    a, b = b, a + b
    if b < limite:
        print(' {}'.format(b), end='')
    else:
        break




