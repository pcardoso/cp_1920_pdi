from math import pi
import math

r = 10

area_1 = pi * r * r
area_2 = math.pi * (r ** 2)

area_1, area_2 = area_2, area_1

print("a area do circulo de raio {} é {}".format(r, area_1))
print("a area do circulo de raio {} é {}".format(r, area_2))