a, b = 1, 1

n = 10

print('1, 1', end='')

for i in range(n - 2):
    a, b = b, a + b
    print(', {}'.format(b), end='')