def imprime_dados(ano, pop_a, pop_b):
    print('ano = {}\n \t pop_a = {} & pop_b = {}'.format(ano, pop_a, pop_b))


pop_a = 80000
pop_b = 200000

ano = 0

imprime_dados(ano, pop_a, pop_b)

while pop_a < pop_b:
    ano += 1
    pop_a *= 1.03
    pop_b *= 1.015
    imprime_dados(ano, pop_a, pop_b)

